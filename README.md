# Elastic K8s

This repository helps with the evaluation of running Elastic Stack on Kubernetes.

# Disclaimer

This project is worked on in my free time and I will not provide support or timely maintenance.
However, please feel free to contribute or submit issues.

# Applying to Kubernetes cluster

This is all series of standalon yaml files describing the cluster.
It usees the elastic kubernetes operation 2.15 to make it easier to connect all the different components within
the clustered service. They should all be applied in order.


```bash
kubectl apply -f ./crds.yaml       # installs the custom resource definitions
kubectl apply -f ./operator.yaml   # installs the operator and its RBAC

kubectl apply -f ./elasticsearch.yaml   # bootstraps an initial elasticsearch cluser - start with 1 instance then upgrade as needed
kubectl apply -f ./kibana.yaml          # starts a kibana service
```

# Accessing cluster data

The Kibana visualization tools is available at [http://localhost/kibana](http://localhost/kibana). Also, the rest endpoints 
for the `elasticsearch` cluster can be used to get data about its documents.

## Raising Issues

Feel free to leave an issue with detailed descriptions of any problems that you have faced with getting your Kubernetes 
cluster created or the elastic stack installed. Summit images in your issue requests or add in logging information from
affected pod as needed.


